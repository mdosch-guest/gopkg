# gopkg

I'm going to collect some little helpers written in [Go](https://golang.org) here.

## jid

Taken from [mellium.im/xmpp/jid](https://mellium.im/xmpp/jid).
License: [BSD-2-Clause](https://opensource.org/licenses/BSD-2-Clause)
